import org.junit.Test;

import static org.junit.Assert.*;

public class testingTest {
    @Test
    public void getName() throws Exception {
        testing rocky = new testing("Iryna");
        assertEquals("Iryna",rocky.getName());

    }

    @Test
    public void testUnhappy() throws Exception {
        testing rocky = new testing("Rocky");
        assertFalse(rocky.isHappy());

    }
}

public class testing {
    private String name;
    private boolean happy = false;

    public testing(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isHappy() {
        return happy;
    }
}
